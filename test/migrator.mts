/**
 * SPDX-PackageName: kwaeri/migrator
 * SPDX-PackageVersion: 0.7.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as assert from 'assert';
import EventEmitter from 'events';
import { MigratorServiceProvider, ExampleMigratorServiceProvider } from '../index.mjs';


// DEFINES
const emitter = new EventEmitter();
const esp = new ExampleMigratorServiceProvider( MigratorServiceProvider.getEmitWrapper( emitter.emit.bind( emitter ) ) );


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'Migrator Functionality Test Suite',
    () => {

        describe(
            'Migrator Get Service Type Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                esp.serviceType,
                                "Migrator Service"
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Get Service Provider Subscriptions Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify(
                                    esp.getServiceProviderSubscriptions()
                                ),
                                JSON.stringify(
                                    { commands: {}, required: {}, optional: {}, subcommands: {} }
                                )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Get Service Provider Help Text Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify(
                                    esp.getServiceProviderSubscriptionHelpText()
                                ),
                                JSON.stringify(
                                    {
                                        helpText: {
                                            "command": "To use this service, read this HelpText."
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Install Migrations Test',
            () => {
                it(
                    'Should assert true, indicating that the migration system installed successfully and returned undefined.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                 await new ExampleMigratorServiceProvider().install(),
                                 undefined
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Check Migrations Install Test',
            () => {
                it(
                    'Should assert true, indicating that the migration system was found to be installed and returned undefined.',
                    async () => {
                        return Promise.resolve(
                            assert.equal (
                                await new ExampleMigratorServiceProvider().checkInstall(),
                                undefined
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Set Service Event Metadata Test',
            () => {
                it(
                    'Should return true, indicating that an emitted ServiceEvent was intercepted and handled by a respective listener.',
                    async () => {
                        emitter.on(
                            "ServiceEvent",
                            ( data: any ) => {
                                return Promise.resolve( assert.equal( JSON.stringify( data ), JSON.stringify( { progressLevel: 50, notice: "Complete", log: "Test", logType: 0 } ) ) )
                            }
                        );

                        esp.setServiceEventMetadata( { progressLevel: 50, notice: "Complete", log: "Test", logType: 0 } );
                    }
                );
            }
        );


    }
);