/**
 * SPDX-PackageName: kwaeri/migrator
 * SPDX-PackageVersion: 0.7.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import { kdt } from '@kwaeri/developer-tools';
import { Filesystem, FilesystemEntry, FilesystemRecord } from '@kwaeri/filesystem';
import { Service, BaseService, BaseServiceProvider, ServicePromiseBits, ServiceProviderSubscriptions, ServiceProviderHelpText, ServiceEventBits, ServiceType, SERVICE_TYPES } from '@kwaeri/service';
import { NodeKitOptions, NodeKitConfigurationBits, NodeKitProjectBits, NodeKitProjectAuthorBits } from '@kwaeri/standards-types';
import debug from 'debug';
import * as Events from 'events';


// DEFINES
const _ = new kdt();

/* Configure Debug module support */
const DEBUG = debug( 'kue:migrator' );


/**
 * Derived type for NodeKit Generator Providers
 */
export type NodeKitMigratorOptions = NodeKitOptions;


/**
 * BaseMigratorService Interface
 *
 * The { BaseMigratorService } is the interface which specifies the implementation
 * expected by all migrator services
 */
export interface BaseMigratorService extends BaseService
{
    checkInstall(): Promise<undefined|null>
    install( options?: any ): Promise<undefined|null>
}


/**
 * Service Class
 *
 * The {@link MibratorService} is the class from which all kue services should inherit
 * from.
 *
 * A service is a resource provided, typically through subsciption to a caller,
 * contracted when an end user requisitions the platform, and will [or should]
 * always need the Events class, and have at least some standard methods.
 *
 * This class helps to sensibly build a derived generator service provider, it will
 * probably only be used directly in special circumstances, and instead - developers
 * would extend from the {@link GeneratorServiceProvider} class.
 */
export abstract class MigratorService extends Filesystem implements BaseMigratorService {

    /**
     * @var { ServiceType }
     */
    type: ServiceType = SERVICE_TYPES.MIGRATOR_SERVICE;

    /**
     * @var { Intl.DateTimeFormatOptions }
     */
    protected timeOptions: Intl.DateTimeFormatOptions = { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false };

    /**
     * @var { ( eventName: string | symbol, ...args: any[] ) => boolean }
     */
    protected emit?: ( ( ( data: ServiceEventBits ) => boolean ) | ( ( data: ServiceEventBits ) => void ) );

    /**
     * The Service class constructor
     * @param { Events.EventEmitter | ( ( data: ServiceEventBits ) => void ) } eventHandler An optional event handler will be composed
     *                                                                                      into the instance by the Steward, but can
     *                                                                                      be passed in directly by a caller (for
     *                                                                                      testing) - and is typed for obvious reasons
     *
     * **Example**
     *
     * A {@link ServiceProvider} could choose to leverage an event-based progress bar, and could `.bind()` to the `.emit()` method - the
     * instance of the *emitter* it's associated with. The emitter would define a **ServiceEvent** type and leverage the `Progress::
     * handler()` to handle those events:
     *
     * ```typescript
     * // Setup
     * const emitter = Events.EventEmitter,
     *      progress = new Progress();
     *
     * // Define the event, binding progress.handler to it
     * emitter.on( "ServiceEvent", progress.handler.bind( progress ) );
     *
     * // Create the service provider, passing in the event emitter's emit, using a special static wrapper method
     * const serviceProvider = new ServiceProvider( ServiceProvider.getEmitWrapper( emitter.emit.bind( emitter ) ) );
     *
     * // Or, leverage a composition-based alternative approach (similar to the steward)
     * const altServiceProvider = new ServiceProvider();
     * altServiceProvider.emit = ServiceProvider.getEmitWrapper( emitter.emit.bind( emitter ) );
     *
     * // Now any call to serviceProvider.updateProgress() is handled
     * serviceProvider.updateProgress( "TAG", { progressLevel: 50, notice: "Processing xyz ..." } );
     * altServiceProvider.updateProgress( "TAG", { progressLevel: 50, notice: "Processing xyz ..." } );
     * ```
     *
     * The other option a {@link ServiceProvider} could choose is to use the progress module directly, and leverage its `getHandler()`
     * convenience method to return a pre-bound handler reference:
     *
     * ```typescript
     * // Setup
     * const progress = new Progress(),
     *      serviceProvider = new ServiceProvider( progress.getHandler() );
     *
     * // Now any call to serviceProvider.updateProgress() is handled
     * ```
     *
     * There are merits to using an event based progress bar - so the option is supported in this fashion. However, a direct
     * route to updating the progress bar was considered - and included - because it does have a more responsive result.
     */
    constructor( handler?: ( ( data: ServiceEventBits ) => boolean ) | ( ( data: ServiceEventBits ) => void ) ) {
        super()

        if( handler )
            this.emit = handler;
    }

    /**
     * The getServiceType getter returns the string {@link ServiceType} of the {@link ServiceProvider}
     *
     * @returns { ServiceType }
     */
    get serviceType(): ServiceType {
        return this.type;
    }

    /**
     * Wraps `Events.EventEmitter.emit()` so that the signature matches the required signature for the
     * constructor's handler argument
     *
     * @param { ( eventName: string | symbol, ...args: any[] ) => boolean } emitFunc The `Events.EventEmitter.emit` being wrapped
     *
     * @returns { ( data: ServiceEventBits ) => void } A wrapped emit to service as a handler for progress bar events
     */
    static getEmitWrapper( emitFunc: ( ( eventName: string | symbol, ...args: any[] ) => boolean ) ): ( data: ServiceEventBits ) => void {
        return ( data: ServiceEventBits ) => emitFunc( "ServiceEvent", data );
    }

    /**
     * Renders the primary function of the {@link Service}.
     *
     * The generics scaffolding help to support advanced extended usage of the Service class. Please see
     * the existing ServiceProviders under the kwaeri scope to see how to get around the
     * generics requirements when you don't have a need for them.
     *
     * @param {NodeKitOptions|any} options An options object, typically an extension of {@link NodeKitOptions}
     *
     * @returns { T } Minimally, an object with a boolean result, and a string type. Empty template provided to allow for type limitation in derived classes..
     */
    abstract renderService<T>( options?: any ): Promise<T>;

    /**
     * Method to emit service event metadata for controlling
     * progress bar display
     *
     * @param { ServiceEventBits } data An object of {@link ServiceEventBits}
     */
    setServiceEventMetadata( data: ServiceEventBits): void {
        // Higher-Order Function fix
        //this.emit!( 'ServiceEvent', data );

        // Allows this change, avoiding if's ofc:
        this?.emit?.( data );
    }

    /**
     * Check install is used by the migrator and migration generator to determine
     * if the migration has been installed yet,
     */
    abstract checkInstall(): Promise<undefined|null>

    /**
     * Install is used by the migrator and migration generator to install the migration
     * system
     */
    abstract install( options?: any ): Promise<undefined|null>
}


// Here we convert all our named derived classes to identically named interfaces such that we're able
// to apply them as mixins, turning our MigratorService class into a class that has all the methods
// of Filesystem and Events.EventEmitter available to it:
//export interface MigratorService extends Filesystem, Events.EventEmitter {};

// We apply our magic here to create the final GeneratorService class:
//_.compose( MigratorService, [ Filesystem, Events.EventEmitter ] );


/**
 * GeneratorServiceProvider Class
 *
 * The {@link MigratorServiceProvider} class with which all kue service providers should extend from,
 * which inherits from the Events and {@link MigratorService} classes and implements all required
 * interfaces for consistency.
 */
export abstract class MigratorServiceProvider extends MigratorService implements BaseServiceProvider {
    /**
     * Gets the subscription object for the service provider, used by the
     * Steward in order to reconcile user-provided commands with the available contracts
     * offered by the currently installed ServiceProviders.
     *
     * @param { any } options A redundant, future proofing, options argument.
     *
     * @return { ServiceProviderSubscriptions } A {@link ServiceProviderSubscriptions} object
     */
    abstract getServiceProviderSubscriptions( options: any ): ServiceProviderSubscriptions;

    /**
     * Gets help text for the {@link MigratorServiceProvider}'s contracts/commands.
     *
     * @param { any} options A redundant, future proofing, options object
     *
     * @returns { ServiceProviderHelpText } An extension of {@link ServiceProviderHelpText}
     */
    abstract getServiceProviderSubscriptionHelpText<T extends ServiceProviderHelpText>( options: any ): T;

    /**
     * Method to simplify emitting progress bar events and debugging
     *
     * @param tag The tag to note for debugging purposes
     * @param serviceEventBits The progress bar event metadata to leverage in setting service event metadata
     *
     * @returns { void }
     */
     updateProgress( tag: String, serviceEventBits: ServiceEventBits ): void {
        DEBUG( `[${tag}] call 'setServiceEventMetadata' with  '${serviceEventBits}` );

        this.setServiceEventMetadata( serviceEventBits );
    }
}


// example of a generator service provider:
export class ExampleMigratorServiceProvider extends MigratorServiceProvider {
    getServiceProviderSubscriptions( options?: any ): ServiceProviderSubscriptions {
        return { commands: {}, required: {}, optional: {}, subcommands: {} };
    }
    getServiceProviderSubscriptionHelpText<T extends ServiceProviderHelpText>( options?: any ): T {
        return { helpText: { "command": `To use this service, read this HelpText.` } } as T;
    }
    async renderService<T extends any>( options?: any ): Promise<T> {
        return Promise.resolve( ( () => console.log( 'Service should run at this point.' ) ) as T );
    }
    async checkInstall(): Promise<undefined | null> {
        return Promise.resolve( undefined as any );
    }
    async install( options?: any ): Promise<undefined | null> {
        return Promise.resolve( undefined as any );
    }
}